from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm

#from django.http import HttpResponse

# Create your views here.
def login(request):
    form = UserCreationForm
    return  render(request, 'login.html', {form:'form'})

def register(request):
    form = UserCreationForm
    return render(request, 'register.html', {form:'form'})
